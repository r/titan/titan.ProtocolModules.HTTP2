= HTTP2 Protocol Modules for TTCN-3 Toolset with Titan, Description
:author: Gábor Szalai
:toc: left

== Functionality

The HTTP2 protocol module implements the message structures of the related protocol <<_4, [4]>> in a formalized way, using the standard specification language TTCN-3. This allows defining of test data (templates) in the TTCN-3 language and correctly encoding/decoding messages when executing test suites using the Titan TTCN-3 test environment.

=== Implemented Protocols

This set of protocol modules implements protocol messages and constants of the HTTP2 protocol as described in https://tools.ietf.org/html/rfc7540[RFC7540].

Header compression is implemented as described in https://tools.ietf.org/html/rfc7541[RFC7541].

==== Modified and Non-Implemented Protocol Elements

None.


=== System Requirements

Protocol modules are a set of TTCN-3 source code files that can be used as part of TTCN-3 test suites only. Hence, protocol modules alone do not put specific requirements on the system used. However, in order to compile and execute a TTCN-3 test suite using the set of protocol modules the following system requirements must be satisfied:

* Titan TTCN-3 Test Executor version CRL 113 200/5 R4A (5.3.pl0) or higher installed. For Installation Guide see <<_2, [2]>>.

NOTE: This version of the test port is not compatible with Titan releases earlier than CRL 113 200/5 R4A.

== Usage

=== Installation

The set of protocol modules can be used in developing TTCN-3 test suites using any text editor; however, to make the work more efficient a TTCN3enabled text editor is recommended (for example `nedit`, `xemacs`). Since the HTTP2 protocol is used as a part of a TTCN-3 test suite, this requires TTCN-3 Test Executor be installed before the module can be compiled and executed together with other parts of the test suite. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

=== Configuration

None.

=== Examples

None.

== Interface Description

=== HTTP2 Frame Handling

The HTTP2 frame is represented by the `HTTP2_Frame` union.

==== HTTP2 Frame Encoding Functions

[source]
f_HTTP2_encode_frame(in HTTP2_Frame pl_frame) return octetstring

Used to encode the `HTTP2_Frame`. Returns the encoded frame in octetstring.

[source]
----
f_HTTP2_decode_frame( in octetstring pl_stream, out HTTP2_Frame pl_frame, out HTTP2_decoder_error_descr pl_error_descr) return integer
----

Used to decode the received HTTP2 frame. If the decoder function detects errors detectable only during decoding, the description of the error is returned via `pl_error_descr`.

Return value:

* `_1_` – Decoding failed.
* `_0_` – Decoding OK

=== Header Compression

The protocol module provides functions and framework for header compression.

A header compression context is represented by the `HTTP2_comp_context` record. Separate context should be maintained for sending and receiving.

How to use the header compression:

1. Create the context with function:
+
[source]
----
HTTP2_comp_context_init(in integer h_table_size_local:=4096, in integer h_table_size_remote:=4096 ) return HTTP2_comp_context
----

2. Encode/Decode every header block with the functions in the exactly the same order as the header blocks are sent or received in order to maintain the header compression context:
+
[source]
----
HTTP2_comp_context_encode(inout HTTP2_comp_context pl_context, in HTTP2_header_block pl_hblock, out octetstring pl_frame_data) return integer

HTTP2_comp_context_decode(inout HTTP2_comp_context pl_context, out HTTP2_header_block pl_hblock, in octetstring pl_frame_data, 
                          in boolean pl_enable_debug:=tsp_HTTP2_comp_context_decode_debug) return integer
----

3. Delete the context with
+
[source]
HTTP2_comp_context_free(inout HTTP2_comp_context pl_context)
+
WARNING: Non-freed context leads to memory leak!!!!

==== Header decompression debug

In order to debug the header decompression the 4th parameter of the function `HTTP2_comp_context_decode` should be set to `true`. Also the `DEBUG` log category should be included to the `FileMask`.

The header decompression debug can be enabled globally by setting the module parameter `tsp_HTTP2_comp_context_decode_debug` to `true`.

== Abbreviations

TTCN-3:: Testing and Test Control Notation version 3

== References

[[_1]]
[1] ETSI ES 201 873-1 v4.4.1 (2012-04) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for TITAN TTCN-3 Test Executor

[[_3]]
[3] Programmer’s Technical Reference for Titan TTCN–3 Test Executor

[[_4]]
[4] https://tools.ietf.org/html/rfc7540[RFC 7540] +
Hypertext Transfer Protocol Version 2 (HTTP/2)

[[_5]]
[5] https://tools.ietf.org/html/rfc7541[RFC 7541] +
HPACK: Header Compression for HTTP/2

